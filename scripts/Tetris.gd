extends Node2D
class_name Tetris

# 0 = empty
# 1 = filled
# 2 = pivot
# the piece's matrix need to have the same size, for rotating them

var PIECE_O = Piece.new(
	[
		[0, 0, 0, 0],
		[0, 1, 2, 0],
		[0, 1, 1, 0],
		[0, 0, 0, 0],
	]
)

var PIECE_I = Piece.new(
	[
		[0, 0, 0, 0],
		[1, 1, 2, 1],
		[0, 0, 0, 0],
		[0, 0, 0, 0],
	]
)

var PIECE_S = Piece.new(
	[
		[0, 0, 0, 0],
		[0, 0, 2, 1],
		[0, 1, 1, 0],
		[0, 0, 0, 0],
	]
)

var PIECE_Z = Piece.new(
	[
		[0, 0, 0, 0],
		[0, 1, 2, 0],
		[0, 0, 1, 1],
		[0, 0, 0, 0],
	]
)

var PIECE_L = Piece.new(
	[
		[0, 0, 0, 0],
		[0, 1, 2, 1],
		[0, 1, 0, 0],
		[0, 0, 0, 0],
	]
)

var PIECE_J = Piece.new(
	[
		[0, 0, 0, 0],
		[0, 1, 2, 1],
		[0, 0, 0, 1],
		[0, 0, 0, 0],
	]
)

var PIECE_T = Piece.new(
	[
		[0, 0, 0, 0],
		[0, 1, 2, 1],
		[0, 0, 1, 0],
		[0, 0, 0, 0],
	]
)

var COLOR_EMPTY = Color(0, 0, 0, 0.1)
var COLOR_FILLED = Color(0, 0, 0, 1)
var COLOR_PIVOT = Color(1, 1, 1, 1)

export var WIDTH:int = 12
export var HEIGHT:int = 14

var board:Board

var cur_piece:Piece
var cur_index:int = 0
var all_pieces:Array = [PIECE_O, PIECE_I, PIECE_S, PIECE_Z, PIECE_L, PIECE_J, PIECE_T]

var gravity_timer:float = 0.0
export var gravity_rate:float = 1.0
export var gravity_multiplier:float = 15

var last_x:int = 0
var horizontal_timer:float = 0.0
export var horizontal_rate:float = 5.0

export var draw_size:float = 32
export var draw_radius:float = 16

var score:int
var highscore:int

func save_game():
	var save_game = File.new()
	save_game.open("user://savegame.save", File.WRITE)
	var data = {"highscore":highscore}
	save_game.store_line(to_json(data))
	save_game.close()

func load_game():
	var save_game = File.new()
	if save_game.file_exists("user://savegame.save"):
		save_game.open("user://savegame.save", File.READ)
		while not save_game.eof_reached():
			var current_line = parse_json(save_game.get_line())
			if current_line != null:
				for i in current_line.keys():
					if i == "highscore":
						highscore = current_line[i]
						print("loeaded highscore: " + str(highscore))
	save_game.close()

func _ready():
	load_game()
	randomize()
	board = Board.new(WIDTH, HEIGHT)
	reset_gravity_timer()
	spawn_new_piece()

func _process(delta: float) -> void:
	if Input.is_action_just_pressed("rotate"):
		rotate_cur_piece()
	var move = get_movement_vector(delta)
	if move != Vector2.ZERO:
		move_cur_piece(move)
	if board.has_lost():
		board.clear()
		spawn_new_piece()
		score = 0
	else:
		score += board.clear_filled_rows()
		if score > highscore:
			highscore = score
			save_game()

func move_cur_piece(move:Vector2) -> void:
	if board.can_place_piece(cur_piece, cur_piece.position_pivot.x + move.x, cur_piece.position_pivot.y + move.y):
		cur_piece.position += move
	elif board.can_place_piece(cur_piece, cur_piece.position_pivot.x, cur_piece.position_pivot.y + move.y):
		cur_piece.position += Vector2(0, move.y)
	else:
		board.place_piece(cur_piece, cur_piece.position_pivot.x, cur_piece.position_pivot.y)
		spawn_new_piece()
	update()

func spawn_new_piece() -> void:
	cur_piece = get_random_piece()
	cur_piece.position = Vector2(board.size.x / 2, 0)

func get_random_piece() -> Piece:
	var i = clamp(rand_range(0, all_pieces.size()), 0, all_pieces.size() - 1)
	return duplicate_piece(all_pieces[i])

func rotate_cur_piece():
	var rotated_piece:Piece = duplicate_piece_and_rotate(cur_piece)
	if board.can_place_piece(rotated_piece, rotated_piece.position_pivot.x, rotated_piece.position_pivot.y):
		cur_piece = rotated_piece
		update()

func get_movement_vector(delta:float) -> Vector2:
	var move = Vector2.ZERO
	# X
	horizontal_timer = clamp(horizontal_timer - delta, 0, horizontal_timer)
	var player_x = 0
	if Input.is_action_pressed("left"):
		player_x -= 1
	if Input.is_action_pressed("right"):
		player_x += 1
	if player_x != 0:
		if horizontal_timer <= 0:
			if board.can_place_piece(cur_piece, cur_piece.position_pivot.x + player_x, cur_piece.position_pivot.y):
				move.x = player_x
			horizontal_timer = 1 / horizontal_rate
		else:
			player_x = 0
	# Y
	var gravity_multi =  1.0
	if Input.is_action_pressed("down"):
		gravity_multi = gravity_multiplier
	gravity_timer -= delta * gravity_multi
	if gravity_timer <= 0:
		reset_gravity_timer()
		move.y = 1
#		if board.can_place_piece(cur_piece, cur_piece.position_pivot.x + player_x, cur_piece.position_pivot.y):
#			move.x = player_x
	# END
	return move

func reset_gravity_timer() -> void:
	gravity_timer = 1 / gravity_rate

func duplicate_piece(piece:Piece) -> Piece:
	# DUPLICATE MATRIX
	var matrix_dup = Array()
	matrix_dup.resize(piece.size)
	for i in matrix_dup.size():
		matrix_dup[i] = Array()
		matrix_dup[i].resize(piece.size)
		for j in piece.size:
			matrix_dup[i][j] = piece.matrix[i][j]
	# CREATE THE NEW PIECE
	var copy = Piece.new(matrix_dup)
	copy.position = piece.position
	return copy

func duplicate_piece_and_rotate(piece:Piece) -> Piece:
	var copy = duplicate_piece(piece)
	copy.rotate_matrix()
	return copy

func _draw() -> void:
	if board != null:
		_draw_board()
	if cur_piece != null:
		_draw_piece()

func _draw_board() -> void:
	for y in board.size.y:
		for x in board.size.x:
			var cell_position = Vector2(x * draw_size, y * draw_size)
			var color = COLOR_EMPTY
			if board.get_cell(x, y) != 0:
				color = COLOR_FILLED
			draw_circle(position + cell_position, draw_radius, color)

func _draw_piece() -> void:
	for x in cur_piece.size:
		for y in cur_piece.size:
			var cell_position = Vector2(x * draw_size, y * draw_size)
#			var color = COLOR_EMPTY
			var color = Color(0, 0, 0, 0)
			if cur_piece.get_cell(x, y) != 0:
				color = COLOR_FILLED
#			if cur_piece.get_cell(x, y) == 2:
#				color = COLOR_PIVOT
			draw_circle(position + cur_piece.position_pivot * draw_size + cell_position, draw_radius, color)