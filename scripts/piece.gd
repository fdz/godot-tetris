extends Reference
class_name Piece

const CELL_PIVOT = 2

var matrix:Array = []
var size:int setget , get_size

var pivot:Vector2
var position:Vector2
var position_pivot:Vector2 setget , get_position_pivot

func _init(p_matrix:Array) -> void:
	matrix = p_matrix
	var N = matrix.size()
	for i in N:
		assert(matrix[i].size() == N)
	find_pivot()

func rotate_matrix() -> void:
	# SOURCE: https://www.geeksforgeeks.org/inplace-rotate-square-matrix-by-90-degrees/
	var N = matrix.size()
	# CONSIDER ALL SQUARES ONE BY ONE
	for x in N / 2:
		# CONSIDER ELEMTNS IN GROUP OF 4 IN CURRENT SQUARE
		for y in range(x, N-x-1):
			# SOTRE CURRENT CELL IN TEMP VARIABLE
			var temp = matrix[x][y]
			# MOVE VALUES FROM RIGHT TO TOP
			matrix[x][y] = matrix[y][N-1-x]
			# MOVE VALUES FROM BOTTOM TO RIGHT
			matrix[y][N-1-x] = matrix[N-1-x][N-1-y]
			# MOVE VALUES FROM LEFT TO BOTTOM
			matrix[N-1-x][N-1-y] = matrix[N-1-y][x]
			# ASSIGN TEMP TO LEFT
			matrix[N-1-y][x] = temp
	find_pivot()

func get_cell(x:int, y:int) -> int:
	return matrix[y][x]

func get_size() -> int:
	return matrix.size()

func find_pivot() -> void:
	for i in matrix.size():
		for j in matrix.size():
			if get_cell(i, j) == CELL_PIVOT:
				pivot = Vector2(i, j)
				return
	assert(false)

func get_position_pivot() -> Vector2:
	return position - pivot

func printme() -> void:
	var s
	for i in matrix.size():
		s = ""
		for j in matrix.size():
			s += str(matrix[i][j])
		print(s)