extends Reference
class_name Board

const CELL_EMPTY:int = 0
const CELL_FILLED:int = 1

var size:Vector2
var cells:Array = []

func _init(width:float, height:float) -> void:
	print("Grid::_init()")
	size = Vector2(width, height)
	for y in size.y:
		var row = Array()
		row.resize(size.x)
		cells.push_back(row)
	set_all_cells(CELL_EMPTY)
	print("\tsize = " + str(size))

func set_all_cells(value:int) -> void:
	for y in size.y:
		for x in size.x:
			set_cell(x, y, value)

func set_cell(x:int, y:int, value:int) -> void:
	cells[y][x] = value

func get_cell(x:int, y:int):
	return cells[y][x]

func is_position_in_bounds(x:int, y:int) -> bool:
	return x >= 0 && y >= 0 && x < size.x && y < size.y

func place_piece(piece:Piece, atX:int, atY:int) -> bool:
	if not can_place_piece(piece, atX, atY):
		return false
	place_piece_unsafe(piece, atX, atY)
	return true

func can_place_piece(piece:Piece, atX:int, atY:int) -> bool:
	for x in piece.size:
		for y in piece.size:
			var curX = atX + x
			var curY = atY + y
			if is_position_in_bounds(curX, curY):
				if piece.get_cell(x, y) != CELL_EMPTY and get_cell(curX, curY) != CELL_EMPTY:
					return false
			else:
				if piece.get_cell(x, y) != CELL_EMPTY:
					return false
	return true

func place_piece_unsafe(piece:Piece, atX:int, atY:int) -> void:
	for x in piece.size:
		for y in piece.size:
			var curX = atX + x
			var curY = atY + y
			if piece.get_cell(x, y) != CELL_EMPTY:
				set_cell(curX, curY, CELL_FILLED)

func remove_piece(piece:Piece, atX:int, atY:int) -> void:
	if not is_position_in_bounds(atX, atY):
		return
	for x in piece.size:
		for y in piece.size:
			var curX = atX + x
			var curY = atY + y
			if is_position_in_bounds(curX, curY) and get_cell(curX, curY) != CELL_EMPTY:
				set_cell(curX, curY, CELL_EMPTY)

func clear_filled_rows() -> int:
	var filled_rows = 0
	for y in size.y:
		var filled_cells = 0
		for x in size.x:
			if get_cell(x, y) != CELL_EMPTY:
				filled_cells += 1
		if filled_cells == size.x:
			filled_rows += 1
			for x in size.x:
				set_cell(x, y, CELL_EMPTY)
			print("row " + str(y) + " cleared")
			var y2 = y
			print("while " + str(y2) + " > 0")
			while y2 > 0:
				for x in size.x:
					var upper_cell = get_cell(x, y2 - 1)
					set_cell(x, y2, upper_cell)
				y2 -= 1
	return filled_rows

func clear() -> void:
	set_all_cells(CELL_EMPTY)

func has_lost() -> bool:
	for x in size.x:
		if get_cell(x, 0) != CELL_EMPTY:
			return true
	return false