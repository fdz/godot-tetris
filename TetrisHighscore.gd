extends Label

export var tetris_path:NodePath

func _process(delta: float) -> void:
	text = "HIGHSCORE: " + str(get_node(tetris_path).highscore)
	text += "\nscore: " + str(get_node(tetris_path).score)